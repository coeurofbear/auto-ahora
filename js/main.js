$( document ).ready(function() {

	$('a[href*="#"]:not([href="#"])').click(function() {
	  if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	    var target = $(this.hash);
	    target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	    if (target.length) {
	      $('html, body').animate({
	        scrollTop: target.offset().top - 160
	      }, 1000);
	      return false;
	    }
	  }
	});
	
	// grab an element
	    var myElement = document.querySelector("nav");
	    // construct an instance of Headroom, passing the element
	    var headroom  = new Headroom(myElement);
	    // initialise
	    headroom.init(); 

});
